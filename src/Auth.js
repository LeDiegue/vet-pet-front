import React from 'react';

import { Route, Redirect } from 'react-router-dom';

const isAuth = () => {
 
    let userRole = JSON.parse(localStorage.getItem('dataUser'));

    if(userRole && userRole.role === 'admin') {
        return true
    }
    return false;
};

const PrivateRoute = ({component: Component, ...rest}) => {
    return (
        <Route 
            {...rest}
            render={props => 
            isAuth() ? (
                <Component {...props} />
            ): (
                <Redirect 
                    to={{
                        pathname: '/login',
                        state: { message: 'Usuario no autorizado' }
                    }}
                />
            )}
        />
    );
}

export default PrivateRoute;