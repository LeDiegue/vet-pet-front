import swal from "sweetalert";

export default function handleError(resp) {
    if(resp.msg){
        swal ( resp.msg ,  "Verifica esos datos para continuar" ,  "error" );
        return;
      }
}