import React, { useState, useEffect } from 'react';
import emailjs from 'emailjs-com';
import './consultacss.css'
import { Redirect, useParams } from 'react-router-dom';
import handleError from "../../../../../Helpers/HandleError";
import Spinner from "../../../../../Helpers/Spinner/Spinner";

// code fragment


var template_params = {
};


export default function ContactUs() {
  const [emailsent, setEmailsent] = useState(false);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();


  useEffect(() => { 
    let status;
  fetch(`http://localhost:4000/faq/${id}`)
  .then(resp => {
    if (!resp.ok) status = resp.status;
    return resp.json();
  })
  .then(resp => {
    if (status) {
      handleError(resp);
      return;
    }
      template_params = {
        ...template_params,
        message_html: resp.inquire,
        id: resp._id,
        name: resp.name,
        urlmail: resp.email
      }
      setLoading(false)
    });
  },[id]);


  function sendEmail(e) {
    e.preventDefault();
    template_params.reply = document.querySelector('#reply').value;
    emailjs.send('gmail', 'mail', template_params, 'user_mOD9ek53cLJ3w29SKdpoV')
      .then((result) => {
        let status;
        fetch(`http://localhost:4000/faq/replyfaq/${template_params.id}`, {
          method: "PUT",
          body: JSON.stringify({ reply: template_params.reply }),
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(resp => {
          if (!resp.ok) status = resp.status;
          return resp.json();
        })
        .then(resp => {
          if (status) {
            handleError(resp);
            return;
          }
          alert("Email enviado correctamente")
          setEmailsent(true)
          });
      }, (error) => {
        console.log(error);
      });
  }

  if (emailsent) {
    return <Redirect to="/admin" />
  }
  
  return (
    <form className="d-flex flex-grow-1 contact-form pt-5" onSubmit={sendEmail}>
      <div class="">
        <div class="">
          <input className="form-control maxwidth " type="hidden" name="contact_number" />
        </div>
        <div class="">
          <h4 className="text-secondary">Consulta de: <span className="text-info">{template_params.name}</span></h4>
        </div>
        <div class="">
          <h4 className="text-secondary">Correo: <span className="text-info">{template_params.urlmail}</span></h4>
        </div>
        <div class=" mt-3">
          <label>Consulta</label>
          <textarea disabled  className="form-control maxwidth" value={template_params.message_html} name="message_html" id="message_html" />
        </div>
        <div class="">
          <label>Respuesta</label>
          <textarea className="form-control maxwidth" name="reply" id="reply" />
        </div>
        <div className=" mt-3 text-right">
          <div className="maxwidth">
            <input className="form-control btn btn-primary btnstyle" type="submit" value="Enviar" />
          </div>
        </div>
      </div>
      {loading ? <Spinner/> : ''}
    </form>
  );
}
