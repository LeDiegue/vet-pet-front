import React, { Component } from 'react';
import handleError from "./../../../../Helpers/HandleError"

export default class componentName extends Component {

  constructor(props) {
    super(props)
    this.state = {
      prodName: '',
      discount: '',
      price: '',
      stock: '',
    };
  }

  handleAddProduct = e => {
    e.preventDefault();
    let status;
    let token = JSON.parse(localStorage.getItem('token'));
    fetch(`http://localhost:4000/products/addproduct`, {
      method: "POST",
      body: JSON.stringify(this.state),
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      },
    })
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          handleError(resp);
          return;
        }
        alert(`Producto Agregado`);
      });
  }

  handleInputChange = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }


  handleImageChange(e) {
    let file = e.target.files[0];
    if (/\.(jpe?g|png|gif)$/i.test(file.name) === false) { alert("No es una imagen!"); return }
    if (file.size > 660000) {
      e.target.value = null
      file = null
      alert("Tamaño del archivo excesivo")
      return;
    }
    e.target.setCustomValidity('');
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      this.setState({
        base64: reader.result
      });
    };
  }


  render() {
    return (
      <>
        <div className="card-body py-5">
          <form className="px-lg-5 w-lg-80" onSubmit={this.handleAddProduct}>
            <div className="w-100 text-center">
              <h2>Agregar producto</h2>
            </div>
            {
              this.state.addProduct ? (
                <div className="alert alert-success mt-3 text-center" role="alert">
                  El producto {this.state.prodName} fue agregado exitósamente! </div>
              ) : ''
            }
            <div className="form-group">
              <label>Titulo</label>
              <input type="text" className='form-control' name="prodName" onChange={this.handleInputChange} />
            </div>
            <div className="form-group">
              <label htmlFor="exampleFormControlFile1">Imagen</label>
              <input type="file" accept="image/jpeg" className="form-control-file" onChange={(e) => this.handleImageChange(e)} />
            </div>
            <div className="form-group">
              <label>Descuento</label>
              <input type="number" name="discount" className='form-control' onChange={this.handleInputChange} />
            </div>
            <div className="form-group">
              <label>Precio</label>
              <input type="number" className='form-control' name="price" onChange={this.handleInputChange} />
            </div>
            <div className="form-group">
              <label>Stock disponible</label>
              <input type="number" className='form-control' name="stock" onChange={this.handleInputChange} />
            </div>
            <div className="form-group form-row mt-4">
              <label className="checkbLabel">Producto Destacado</label>
              <input type="checkbox" className='form-control checkb' name="featured" onChange={this.handleInputChange} />
            </div>
            <button type="submit" className="btn btn-success mt-4">Agregar producto</button>
          </form>
        </div>
      </>
    )
  }
}

