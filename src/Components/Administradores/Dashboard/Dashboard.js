import React, { Component } from 'react';
import './Dashboard.css';
import $ from 'jquery';
import MyProducts from './ComponentsDashboard/MyProducts';
import AddProducts from './ComponentsDashboard/AddProducts';
import ConsultasPanel from './ComponentsDashboard/ConsultasPanel/ConsultasPanel';
import Message from './ComponentsDashboard/Message';
import EditProduct from './ComponentsDashboard/EditProduct';
import DeleteProduct from './DeleteProduct';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";


export default class LoggedIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      admiLogged: '',
      logout: false
    }
  }


  componentDidMount() {
    const admin = JSON.parse(localStorage.getItem('dataUser'));
    this.setState({ admiLogged: admin })
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }


  adminLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('dataUser');
    this.setState({
      logout: true
    })
  }


  render() {
    if (this.state.logout) {
      return <Redirect to={'/login'} />
    }

    return (
      <React.Fragment>
        <Router>
          <div className="d-flex" id="wrapper">
            <div className="bg-light border-right" id="sidebar-wrapper">
              <div className="sidebar-heading">Bienvenido</div>
              <div className="list-group list-group-flush">
                <Link to="/admin/messages" className="list-group-item list-group-item-action bg-light">Mensajes</Link>
                <Link to="/admin/addproducts" className="list-group-item list-group-item-action bg-light">Agregar productos</Link>
                <Link to="/admin/myproducts" className="list-group-item list-group-item-action bg-light">Ver productos</Link>
              </div>
            </div>
            <div id="page-content-wrapper" className=" d-flex flex-column">
              <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <button className="btn btn-info" id="menu-toggle"><i className="fas fa-bars"></i></button>
                <div className="nav-item dropdown ml-auto">
                  <button className="nav-link border-0 dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {this.state.admiLogged.name}
                  </button>
                  <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <button className="dropdown-item" onClick={this.adminLogout}>Salir</button>
                  </div>
                </div>
              </nav>

              <div className="flex-grow-1 d-flex flex-column">
                <Switch>

                  <Route path="/admin/deleteproducts/:id">
                    <DeleteProduct />
                  </Route>

                  <Route path="/admin/editproducts/:id">
                    <EditProduct />
                  </Route>

                  <Route path="/admin/myproducts">
                    <MyProducts />
                  </Route>

                  <Route path="/admin/addproducts">
                    <AddProducts />
                  </Route>

                  <Route path="/admin/messages/:id">
                    <ConsultasPanel />
                  </Route>

                  <Route path="/">
                    <Message />
                  </Route>

                </Switch>
              </div>
            </div>
          </div>
        </Router>
      </React.Fragment>
    )
  }
}
