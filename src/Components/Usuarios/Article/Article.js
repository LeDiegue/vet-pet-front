import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import './Article.css';

export default function Article() {
  const coleccionCosas = JSON.parse(localStorage.getItem('CarritoItems')) || [];
  const [itemAmount, setItemAmount] = useState(1);
  const [articleInfo, setArticleInfo] = useState({
    articleInfo: ''
  });
  const parametro = useParams().id;
  
  useEffect(() => {
    fetch('http://localhost:4000/products/search/oneproduct/' + parametro)
      .then(resp => resp.json())
      .then(data => setArticleInfo(data));
  }, [parametro]);

  function isAdded() {
    let exist = coleccionCosas.find((item) => {
      return item.id === articleInfo.id;
    });
    return exist ? true : false
  }
  const handleInput =
    (e) => {
      const value = e.target.value;
      setItemAmount(value)
    }
  function handleClick() {
    const article = articleInfo;
    article.quantity = itemAmount
    coleccionCosas.push(article);
    save(coleccionCosas);
  }
  function save(it) {
    localStorage.setItem('CarritoItems', JSON.stringify(it));
  }

  let a = ''
  if (isAdded()) {
    a = <button className="slide-top align-self-center border-0 p-0 bg-white">
      <img title="Item Added to the Cart :D" src="https://img.icons8.com/color/40/000000/checked-2.png" alt="tildado"/>
    </button>;
  } else {
    a = <button onClick={() => handleClick()} className="align-self-center border-0 p-0 bg-white">
      <img title="Add to the cart" src="https://img.icons8.com/nolan/40/favorite-cart.png" alt="carrito" />
      </button>;
  }
  return (
    <React.Fragment>
      <section className="container">
        <div className="row justify-content-between my-5">
          <div className="col-4">
            {articleInfo.discount ? (
              <div className="discount-label yellow">
                {" "}
                <span>-{articleInfo.discount}%</span>
              </div>
            ) : (
                ""
              )}
            <div className="card card-style">
              <img alt="imagen de producto" src={articleInfo.image} className="card-img-top img-fluid style-img-items" />
              <div className="card-body">
                <div className='row'>
                  <div className="col-8">
                    <h5 className="card-title">{articleInfo.prodName}</h5>
                  </div>
                  <div className="col-12 mb-2 d-flex justify-content-between input-group-prepend box-Cart">
                    {a}
                    <select name="quantity" className="form-control inputNumberButtons w-25 text-center" onChange={handleInput}>
                      <option value="1" defaultValue>1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                    </select>
                  </div>
                  <div className="col-12">
                    <span className="style-price-span box-price">Precio: ${articleInfo.price}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5">
            <div className="card table-bordered text-align-center">
              <div className="card-header ">
                El siguiente boton te va a redirigir a Mercado Pago
            </div>
              <div className="card-body row">
                <button onClick={handleClick} className="border-0 text-white buyBtn m-auto">
                  Finalizar Compra!
              </button>
              </div>
            </div>
          </div>
        </div>


      </section>

    </React.Fragment>

  );
}

