// import React from "react";
// import NavLinksBar from "./../NavLinksBar/NavLinksBar";
// import CommonNavbar from "./../CommonNavbar/CommonNavbar";
// import Slider from "./../Slider/Slider";
// import Servicios from "./../Servicios/Servicios";
// import ServiciosRuta from "./../ServiciosRuta/ServiciosRuta";
// import Article from "./../Article/Article";
// import MiniMarket from "./../MiniMarket/MiniMarket";
// import handleError from "./../../../Helpers/HandleError";

// import OurTeam from './../OurTeam/OurTeam';
// import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// // import { HashLink as Link } from 'react-router-hash-link';


// export default class UserDashboard extends React.Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       userLogged: JSON.parse(localStorage.getItem("token")),
//       featured: [],
//       loggedIn: ""
//     };
//   }

//   getFeatured() {
//     let status;
//     fetch("http://localhost:4000/products/search/featured", {})
//       .then(resp => {
//         if (!resp.ok) status = resp.status;
//         return resp.json();
//       })
//       .then(resp => {
//         // this.setState({ loading: "" });
//         if (status) {
//           handleError(resp);
//           return;
//         }
//         this.setState({ featured: resp.prods });
//       });
//     // .catch(error => {
//     //   console.log(error);
//     // })
//   }

//   handleUserLogged = item => {
//     this.setState({
//       userLogged: item
//     });
//   };
//   componentDidMount() {
//     this.getFeatured();
//   }

//   handleTurnoDispo = date => {
//     console.log(date);

//     fetch(`http://localhost:4000/appointment/${date}`)
//       .then(resp => resp.json())
//       .then(data => {
//         console.log(data);
//       });
//   };

//   handleTurno = item => {
//     console.log(item);

//     fetch(`http://localhost:4000/appointment`, {
//       method: "POST",
//       body: JSON.stringify({ startTime: item }),
//       headers: {
//         "Content-Type": "application/json"
//       }
//     })
//       .then(resp => resp.json())
//       .then(data => {
//         alert("TU PEDIDO HA SIDO ENVIADO, AGUARDA CONFIRMACION DE TURNO");
//       });
//   };

//   handleContacto = item => {
//     fetch(`http://localhost:4000/contacto`, {
//       method: "POST",
//       body: JSON.stringify(item),
//       headers: {
//         "Content-Type": "application/json"
//       }
//     })
//       .then(resp => resp.json())
//       .then(data => {
//         alert("Gracias x tu consulta, responederemos a la brevedad");
//       });
//   };

//   render() {
//     const data = this.state.featured;

//     console.log(this.state.userLogged);
//     return (
//       <Router>
//         <div>
//           <Switch>
//             <Route path="/article/:id">
//               <CommonNavbar />
//               <Article />
//             </Route>
//             <Route path="/servicios">
//               <CommonNavbar />
//               <ServiciosRuta />
//             </Route>


//             <Route path="/">
//               <NavLinksBar
//                 loggedIn={this.state.userLogged}
//                 userLogged={this.handleUserLogged}
//               />
//               <Slider />
//               <MiniMarket data={data} />
//               <Servicios />
//               <OurTeam />
//             </Route>
//           </Switch>
//         </div>
//       </Router>
//     );
//   }
// }
