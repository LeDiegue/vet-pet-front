import React, { Component } from "react";
import "./SocialMediaMIndex.css";

export default class SocialMediaMIndex extends Component {
  render() {
    return (
      <div>
        <div className=" rrss">
          <div className="d-none d-md-flex  ">
            <nav className="social ">
              <ul>
                <li className="col-12 pt-1">
                  <a className="d-flex" href="https://twitter.com/">
                    <i className="col-4 fab fa-twitter"></i>
                    <p className="col-7 pt-1">Twitter</p>
                  </a>
                </li>
                <li className="col-12 my-1 pt-1">
                  <a
                    className="d-flex"
                    href="https://www.facebook.com/Patitas-Veterinaria-108947807363818/"
                  >
                    <i className="col-4 fab fa-facebook "></i>

                    <p className="col-7 pt-1">Facebook</p>
                  </a>
                </li>

                <li className="col-12 pt-1">
                  <a className="d-flex" href="http://behance.net/gianm">
                    <i className="col-4 fab fa-whatsapp  "></i>{" "}
                    <p className="col-7 pt-1">WhatsApp</p>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    );
  }
}
