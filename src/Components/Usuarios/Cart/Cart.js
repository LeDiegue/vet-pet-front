import React, { useState } from "react";
import swal from "sweetalert";
import { Redirect } from "react-router-dom";
import "./Cart.css";

export default function Cart(props) {
  const [coleccionCosas, setColeccionCosas] = useState(
    JSON.parse(localStorage.getItem("CarritoItems")) || []
  );

  const [redirectLogin, setRedirectLogin] = useState(false)

  const coleccion = coleccionCosas.map(element => {
    return { prodId: element._id, count: parseInt(element.quantity) };
  });

  function handleClickRemove(i) {
    coleccionCosas.splice(i, 1);
    localStorage.setItem("CarritoItems", JSON.stringify(coleccionCosas));
    setColeccionCosas(JSON.parse(localStorage.getItem("CarritoItems")));
  }
  
  const token = JSON.parse(localStorage.getItem("token"));
  const handleClick = () => {
    if (token) {
      if (coleccion.length !== 0) {
        fetch("http://localhost:4000/sales/addsale", {
          method: "POST",
          body: JSON.stringify({ products: coleccion }),
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          }
        })
          .then(resp => resp.json())
          .then(res => (window.location.href = res.link))
          .catch(err => console.log(err));
      } else {
        swal ( "Tu Carrito esta vacio" ,  "Verifica esos datos para continuar" ,  "error" );
        return;
      }
    }else{
      swal ( "No estas Logueado" ,  "Verifica esos datos para continuar" ,  "error" )
      .then(() =>{
        setRedirectLogin(true)
      });
      return;
    }
  };

  const CartTable =
    Array.isArray(coleccionCosas) &&
    coleccionCosas.map((item, i) => {
      return (
        <tr>
          <th colSpan="1" scope="row" className="text-center">
            <img alt="imagen de producto" src={item.image} className="img-fluid img" />
          </th>
          <td colSpan="4">{item.prodName}</td>
          <td>x{item.quantity}</td>
          <td>${item.price}</td>
          <td>{item.discount}%</td>
          <td>
            <button
              onClick={() => handleClickRemove(i)}
              className="btn btn-danger"
            >
              X
            </button>
          </td>
        </tr>
      );
    });

    if(redirectLogin){
      return <Redirect to="/login"/>
    }
  return (
    <div className="container  mt-3">
      <div className="row justify-content-between">
        <div className="col-12 col-md-7 table-responsive">
          <table className="table  table-bordered">
            <thead>
              <tr>
                <th scope="row"></th>
                <th colSpan="4">Tu Carrito</th>
                <th scope="col">C/D</th>
                <th scope="col">Precio</th>
                <th scope="col">Desc</th>
                <th scope="col">Borrar</th>
              </tr>
            </thead>
            <tbody className="widthImg">{CartTable}</tbody>
          </table>
        </div>
        <div className="col-12 col-md-5">
          <div className="card table-bordered text-align-center">
            <div className="card-header ">
              El siguiente boton te va a redirigir a Mercado Pago
            </div>
            <div className="card-body row">
              <button onClick={handleClick} className="border-0 text-white buyBtn m-auto">
                Finalizar Compra!
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
