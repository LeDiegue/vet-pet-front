import React from "react";
import "./Footer.css";
import "images/LOGO_VETE.svg";
// import { BrowserRouter as Link } from "react-router-dom";
import { HashLink as Link } from "react-router-hash-link";

function Footer() {
  /*  function goo(id){
    var scrollDiv = document.getElementById(id).offsetTop;
    window.scrollTo({ top: scrollDiv, behavior: 'smooth'});
  }
  no andan los links cuando en otro component HAY Q SOLUCIONAR */
  return (
    <React.Fragment>
      <div className=" sticky container-fluid">
        <hr className="line"></hr>

        <div className="row footerlinks align-items-center">
          <div className="col-12 col-md-3 order-md-1 order-sm-2 order-2 logo-footer text-center">
            <Link className="" to="/">
              <img alt="logo patitas" src={"images/LOGO_VETE.svg"} />
            </Link>
          </div>

          <div className="col-12 col-md-3 order-md-2 order-sm-0 order-xs-0 pt-sm-3  py-xs-2 py-sm-2 my-xs-2 my-sm-2 footer-listfont effect-underline ">
            <li>
              <Link
                className="footerlink effect-underline"
                to={{ pathname: "/contacto", hash: "#contacto" }}
              >
                CONSULTAS
              </Link>
            </li>
            <li>
              <Link
                className="footerlink effect-underline"
                to={{ pathname: "/turnos", hash: "#turnos" }}
              >
                TURNOS
              </Link>
            </li>
            <li>
              <Link
                className="footerlink effect-underline"
                to={{ pathname: "/market", hash: "#market" }}
              >
                MARKET
              </Link>
            </li>
            <li>
              <Link
                className="footerlink effect-underline"
                to={{ pathname: "/ourteam", hash: "#ourteam" }}
              >
                NUESTRO EQUIPO
              </Link>
            </li>
          </div>
          <ul className="col-12 col-md-3 order-md-3 order-sm-1 order-xs-1 py-3  my-xs-2 my-sm-2 footer-icons d-flex justify-content-center align-items-center sibling-fade pr-3 ">
            <i className="fab fa-instagram px-1"></i>
            <a
              href="https://www.facebook.com/Patitas-Veterinaria-108947807363818/"
              target="_blank" rel="noopener noreferrer"
            >
              {" "}
              <i className="fab fa-facebook px-1"></i>
            </a>
            <i className="fab fa-whatsapp  px-1"></i>
            <i className="fab fa-twitter px-1"></i>
          </ul>
          <div className="col-12 col-md-3 order-md-4 d-none d-md-block footer-map p-3">
            <iframe title="gmaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4892.951901299375!2d1.4266911575665824!3d38.907919034458146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1b18f579253d8380!2sVila%20Veterinaris!5e0!3m2!1ses!2sar!4v1580473217396!5m2!1ses!2sar"></iframe>
          </div>
        </div>
      </div>
      <div className="copyright text-center">
        <p>RollingCode School 2020 - Comision1 ©</p>
      </div>
    </React.Fragment>
  );
}

export default Footer;
