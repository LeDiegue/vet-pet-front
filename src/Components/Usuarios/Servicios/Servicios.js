import React from "react";
import "./Servicios.css";
import { Link } from "react-router-dom";

const dataServicios = [
  {
    id: 1,
    img:
      "https://kitteryanimalhospital.com/wp-content/uploads/2018/06/Emergency-Urgent-Care.jpg",
    text: "CLINICA",
    URL: "/Contacto"
  },
  {
    id: 2,
    img:
      "https://contestimg.wish.com/api/webimage/5645b0edcaa28515da05facf-large.jpg?cache_buster=e5a412432db324d45cc08ac978be4f4d",
    text: "PELUQUERIA",
    URL: "/Turnos"
  },
  {
    id: 3,
    img:
      "https://i.pinimg.com/736x/b8/25/c9/b825c9ad6e9e8c55412dfc2e1d192a8d.jpg",
    text: "MARKET",
    URL: "/Market"
  },
  {
    id: 4,
    img:
      "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2019/06/25094417/GettyImages-6192660241-500x500.jpg",
    text: "ALIMENTOS",
    URL: "/Market"
  },
  {
    id: 5,
    img: "https://www.uab.cat/Imatge/567/888/HCV7domino.jpg",
    text: "CIRUGIAS",
    URL: "/Contacto"
  },
  {
    id: 6,
    img:
      "https://www.balancedlifepet.com.au/wp-content/uploads/2019/02/pregnant-dog-ultrasound.jpg",
    text: "LABORATORIO",
    URL: "/Contacto"
  }
];

export default function servicios(props) {
  const servicios = dataServicios.map((data, i) => (
    <React.Fragment key={i}>
      <div className="col-6 col-md-4 mb-4 containerserv">
        <div className="positionserv middle1">
          <Link to={data.URL} className="text">
            {" "}
            {data.text}{" "}
          </Link>
        </div>
        <img  alt={"imagen de servicio " + data.text} src={data.img} className="card-img-top image" />
      </div>
    </React.Fragment>
  ));

  return (
    <React.Fragment>
      <div className="overhiden">
        <div className="banner-servicios container-servicios text-center">
          <h1>NUESTROS SERVICIOS</h1>
        </div>

        <div className="container-fluid text-center my-5 ">
          <div className=" ourservices ">
            <p className="mt-4 justify-content-center  ">
              <span>PATITAS CLINICA VETERINARIA</span> nace con la vocación de
              prestar un servicio veterinario de excelente calidad en la ciudad
              de San Miguel de Tucumán. Para ello, la clínica cuenta con el
              mejor equipamiento y su equipo humano esta compuesto por
              profesionales de gran experiencia y extensa formación. Esperamos
              brindarles la mejor experiencia en cada visita a nuestros clientes
              y sus dueños!
            </p>
          </div>
          <div className=" container mt-5 ">
            <div className=" row justify-content-around  ">{servicios}</div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
